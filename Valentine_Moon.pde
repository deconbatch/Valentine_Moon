/**
 * Valentine Moon.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.09.20
 * It generates something like fruits and leaves under the pale light.
 * 
 *
 */

void setup() {

  size(980, 980);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();

}

void draw() {

  int   imgMax  = 3;
  int   ptnMax  = 3;
  int   plotMax = 700;
  float hueInit = random(360.0);

  for (int imgCnt = 0; imgCnt < imgMax; ++imgCnt) {
    // shape parameters
    float paramA   = random(4.0, 5.0);
    float paramB   = random(1.0, 3.0);
    float paramC   = random(4.0, 5.0);
    float paramD   = random(1.0, 3.0);
    float plotMult = random(3.0, 4.0);
    float plotDiv  = random(0.0001, 0.0002);

    hueInit += 90.0;
    background(hueInit, 90.0, 20.0, 100);
    blendMode(LIGHTEST);
    noStroke();

    for (int ptnCnt = 0; ptnCnt < ptnMax; ++ptnCnt) {
      float ptnRatio = map(ptnCnt, 0, ptnMax, 0.0, 1.0);

      float initDiv = map(ptnRatio, 0.0, 1.0, 0.1, 0.05);
      float baseHue = hueInit + map(ptnRatio, 0.0, 1.0, 120.0, 0.0);
      float baseSiz = map(ptnRatio, 0.0, 1.0, 60.0, 30.0);
     
      // draw vector field
      for (float xInit = 0.0; xInit < 1.0; xInit += initDiv) {
        for (float yInit = 0.0; yInit < 1.0; yInit += initDiv) {
          float xPoint = xInit;
          float yPoint = yInit;
          for (int plotCnt = 0; plotCnt < plotMax; ++plotCnt) {
            float plotRatio = map(plotCnt, 0, plotMax, 0.0, 1.0);
            float sineRatio = sin(PI * plotRatio);
            
            float eHue = baseHue + plotRatio * 30.0 + noise(xInit, yInit) * 60.0;
            float eSat = map(sineRatio, 0.0, 1.0, 100.0, 10.0);
            float eBri = map(sineRatio, 0.0, 1.0, 100.0, 0.0);
            float eSiz = baseSiz * sineRatio;

            float xPrev = xPoint;
            float yPrev = yPoint;
            xPoint += plotDiv * (sin(TWO_PI * paramA * yPrev * plotMult) - cos(TWO_PI * paramB * xPrev * plotMult));
            yPoint += plotDiv * (sin(TWO_PI * paramC * xPrev * plotMult) - cos(TWO_PI * paramD * yPrev * plotMult));

            fill(eHue % 360.0, eSat, eBri, 80.0);
            ellipse(xPoint * width, yPoint * height, eSiz, eSiz);
          }
        }
      }
    }
  
    casing();
    saveFrame("frames/" + String.format("%04d", imgCnt + 1) + ".png");

  }
  exit();

}

/**
 * casing : draw fancy casing
 */
private void casing() {
  blendMode(BLEND);
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(60.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
}
